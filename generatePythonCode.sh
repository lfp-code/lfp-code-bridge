set -e
trap 'rc=$?; echo "error code $rc at $LINENO"; exit $rc' ERR

URL="https://repo1.maven.org/maven2/io/grpc/protoc-gen-grpc-java/1.41.0/protoc-gen-grpc-java-1.41.0-windows-x86_64.exe"
FILE_PROTO="bridge_service.proto"

DIR_LINUX="$(dirname "$(realpath "$0")")"
FILE_PLUGIN_LINUX="$DIR_LINUX/temp/protoc-gen-grpc-java.exe"

if [ ! -f $FILE_PLUGIN_LINUX ]; then
	mkdir -p $(dirname ${FILE_PLUGIN_LINUX})
	wget "$URL" -O $FILE_PLUGIN_LINUX
	chmod +x $FILE_PLUGIN_LINUX
fi

DIR_WIN=$(wslpath -w $DIR_LINUX)
FILE_PLUGIN_WIN=$(wslpath -w $FILE_PLUGIN_LINUX)

DIR_PROJECT_WIN="$DIR_WIN\impl"
echo "DIR_PROJECT_WIN=$DIR_PROJECT_WIN"

DIR_PROTO_WIN="$DIR_PROJECT_WIN\src\main\resources\proto"
echo "DIR_PROTO_WIN=$DIR_PROTO_WIN"

DIR_PYTHON_OUT_WIN="$DIR_WIN\python\src\main\resources\com\lfp\code\bridge\python"
echo "DIR_PYTHON_OUT_WIN=$DIR_PYTHON_OUT_WIN"


#pip.exe install grpcio grpcio-tools
python.exe -c "import grpc"
if [ $? -ne 0 ]; then
    pip.exe install grpcio
fi
pip.exe install grpcio-tools

python.exe \
	-m grpc_tools.protoc \
	-I $DIR_PROTO_WIN \
	--python_out=$DIR_PYTHON_OUT_WIN \
	--grpc_python_out=$DIR_PYTHON_OUT_WIN \
	$FILE_PROTO

