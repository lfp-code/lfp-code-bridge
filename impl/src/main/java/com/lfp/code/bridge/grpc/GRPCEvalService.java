package com.lfp.code.bridge.grpc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.Optional;

import org.reactivestreams.Publisher;

import com.google.common.reflect.TypeToken;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.lfp.code.bridge.CodeBridgeServer;
import com.lfp.code.bridge.CodeBridgeService;
import com.lfp.code.bridge.EvalRequest;
import com.lfp.code.bridge.EvalService;
import com.lfp.code.bridge.grpc.bridgeservice.ReactorBridgeServiceGrpc;
import com.lfp.code.bridge.grpc.bridgeservice.ReactorBridgeServiceGrpc.ReactorBridgeServiceStub;
import com.lfp.code.bridge.grpc.bridgeservice.Request;
import com.lfp.code.bridge.grpc.bridgeservice.Response;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.grpc.GRPCClient;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import io.grpc.ManagedChannel;
import reactor.core.publisher.Flux;

public abstract class GRPCEvalService<CB extends CodeBridgeService> extends EvalService.Abs<CB, Response> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String BRIDGE_SERVICE_NAME = "bridge_service.proto";
	private final MemoizedSupplier<File> bridgeServiceProtoFileSupplier = Utils.Functions.memoize(() -> {
		return createBridgeServiceProtoFile();
	}, null);
	private final GRPCClient<ReactorBridgeServiceStub> client;

	public GRPCEvalService(CB service, String hostname, Integer port, String password, Duration refreshAfter,
			String... packageNames) {
		this(service, CodeBridgeServer.toInetSocketAddress(hostname, port), password, refreshAfter, packageNames);
	}

	public GRPCEvalService(CB service, InetSocketAddress inetSocketAddress, String password, Duration refreshAfter,
			String... packageNames) {
		super(service, inetSocketAddress, password, refreshAfter, packageNames);
		this.client = new GRPCClient<ReactorBridgeServiceGrpc.ReactorBridgeServiceStub>(this.getServerAddress(),
				this.getPassword().orElse(null),
				cb -> cb.usePlaintext()) {

			@Override
			protected ReactorBridgeServiceStub createService(ManagedChannel channel) {
				return ReactorBridgeServiceGrpc.newReactorStub(channel);
			}
		};
		this.onScrap(this.client);
		this.client.onScrap(this);
	}

	@Override
	protected Publisher<Response> evalInternal(EvalRequest evalRequest) throws IOException {
		var bytes = Utils.Bits.from(evalRequest.getCodeSource().get());
		var data = Serials.Gsons.get().toJson(bytes.encodeCharset(Utils.Bits.getDefaultCharset()));
		var message = Request.newBuilder().setData(data).build();
		return Flux.just(message).transform(this.client.getService()::invoke);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected <X> X mapResponse(Response response, TypeToken<X> typeToken) throws Exception {
		if (response == null)
			return null;
		var errorOp = parseError(response);
		if (errorOp.isPresent())
			throw new Exception(errorOp.get());
		JsonElement data = Optional.ofNullable(response.getData())
				.map(Serials.Gsons.getJsonParser()::parse)
				.orElse(null);
		if (data == null)
			data = JsonNull.INSTANCE;
		return Serials.Gsons.get().fromJson(data, typeToken.getType());
	}

	@SuppressWarnings("deprecation")
	protected Optional<String> parseError(Response response) {
		if (!Boolean.TRUE.equals(response.getError()))
			return Optional.empty();
		var data = response.getData();
		if (Utils.Strings.isNotBlank(data)) {
			var dataJe = Utils.Functions.catching(data, Serials.Gsons.getJsonParser()::parse, t -> null);
			if (dataJe != null)
				data = Serials.Gsons.tryGetAsString(dataJe).orElseGet(() -> dataJe.toString());
		}
		if (Utils.Strings.isBlank(data))
			data = "unknown error";
		return Optional.of(data);
	}

	protected GRPCClient<ReactorBridgeServiceStub> getClient() {
		return this.client;
	}

	protected File createBridgeServiceProtoFile() throws IOException {
		var file = Utils.Functions.catching(() -> Utils.Resources.getResourceFile(BRIDGE_SERVICE_NAME), t -> null);
		if (file != null && file.exists())
			return file;
		file = Utils.Files.tempFile(THIS_CLASS,
				String.format("temp_%s_%s", Utils.Crypto.getRandomString(), BRIDGE_SERVICE_NAME));
		this.onScrap(file::delete);
		try (var is = Utils.Resources.getResourceInputStream(BRIDGE_SERVICE_NAME);
				var fos = new FileOutputStream(file)) {
			Utils.Bits.copy(is, fos);
		}
		return file;

	}

	@Override
	protected Iterable<? extends Object> getArguments() throws IOException {
		return getArguments(this.bridgeServiceProtoFileSupplier.get());
	}

	protected abstract Iterable<? extends Object> getArguments(File protoFile) throws IOException;

}
