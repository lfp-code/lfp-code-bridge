package com.lfp.code.bridge.grpc.bridgeservice;

import static com.lfp.code.bridge.grpc.bridgeservice.BridgeServiceGrpc.getServiceDescriptor;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;


@javax.annotation.Generated(
value = "by ReactorGrpc generator",
comments = "Source: bridge_service.proto")
public final class ReactorBridgeServiceGrpc {
    private ReactorBridgeServiceGrpc() {}

    public static ReactorBridgeServiceStub newReactorStub(io.grpc.Channel channel) {
        return new ReactorBridgeServiceStub(channel);
    }

    public static final class ReactorBridgeServiceStub extends io.grpc.stub.AbstractStub<ReactorBridgeServiceStub> {
        private BridgeServiceGrpc.BridgeServiceStub delegateStub;

        private ReactorBridgeServiceStub(io.grpc.Channel channel) {
            super(channel);
            delegateStub = BridgeServiceGrpc.newStub(channel);
        }

        private ReactorBridgeServiceStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
            delegateStub = BridgeServiceGrpc.newStub(channel).build(channel, callOptions);
        }

        @Override
        protected ReactorBridgeServiceStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new ReactorBridgeServiceStub(channel, callOptions);
        }

        public reactor.core.publisher.Flux<com.lfp.code.bridge.grpc.bridgeservice.Response> invoke(reactor.core.publisher.Flux<com.lfp.code.bridge.grpc.bridgeservice.Request> reactorRequest) {
            return com.salesforce.reactorgrpc.stub.ClientCalls.manyToMany(reactorRequest, delegateStub::invoke, getCallOptions());
        }

    }

    public static abstract class BridgeServiceImplBase implements io.grpc.BindableService {

        public reactor.core.publisher.Flux<com.lfp.code.bridge.grpc.bridgeservice.Response> invoke(reactor.core.publisher.Flux<com.lfp.code.bridge.grpc.bridgeservice.Request> request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
                    .addMethod(
                            com.lfp.code.bridge.grpc.bridgeservice.BridgeServiceGrpc.getInvokeMethod(),
                            asyncBidiStreamingCall(
                                    new MethodHandlers<
                                            com.lfp.code.bridge.grpc.bridgeservice.Request,
                                            com.lfp.code.bridge.grpc.bridgeservice.Response>(
                                            this, METHODID_INVOKE)))
                    .build();
        }

        protected io.grpc.CallOptions getCallOptions(int methodId) {
            return null;
        }

    }

    public static final int METHODID_INVOKE = 0;

    private static final class MethodHandlers<Req, Resp> implements
            io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
        private final BridgeServiceImplBase serviceImpl;
        private final int methodId;

        MethodHandlers(BridgeServiceImplBase serviceImpl, int methodId) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                default:
                    throw new java.lang.AssertionError();
            }
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                case METHODID_INVOKE:
                    return (io.grpc.stub.StreamObserver<Req>) com.salesforce.reactorgrpc.stub.ServerCalls.manyToMany(
                            (io.grpc.stub.StreamObserver<com.lfp.code.bridge.grpc.bridgeservice.Response>) responseObserver,
                            serviceImpl::invoke, serviceImpl.getCallOptions(methodId));
                default:
                    throw new java.lang.AssertionError();
            }
        }
    }

}
