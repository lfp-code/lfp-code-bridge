package test;

import com.google.common.reflect.TypeToken;
import com.google.gson.JsonElement;
import com.lfp.code.bridge.EvalRequest;
import com.lfp.code.bridge.nodejs.NodeJSEvalService;

public class NodeJSEvalServiceTest2 {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws InterruptedException {
		var service = new NodeJSEvalService();
		String script = "console.log('test'); \n return new Promise(resolve => setTimeout(resolve, 5_000)).then(async () => {var out=[];for(i=0;i<10;i++){ out.push('pass:'+i);} return out;});";
		//script = "console.log('test'); \n return 5*5;";
		var flux = service.eval(EvalRequest.builder(script).returnsVoid(true).build(), TypeToken.of(JsonElement.class));
		flux = flux.doOnNext(v -> {
			System.out.println(v);
		});
		flux.block();
		System.out.println("request complete");
		Thread.currentThread().join();
	}
}
