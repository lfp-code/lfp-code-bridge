package com.lfp.code.bridge.nodejs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.Files;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.commons.lang3.Validate;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.listener.ProcessListener;
import org.zeroturnaround.zip.ZipUtil;

import com.lfp.code.bridge.CodeBridgeContext;
import com.lfp.code.bridge.CodeBridgeService;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.process.ProcessLFP;
import com.lfp.joe.process.Procs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;

import one.util.streamex.StreamEx;

public class NodeJSCodeBridgeService extends CodeBridgeService.Abs {

	public static NodeJSCodeBridgeService get() {
		return Instances.get();
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	// esm doesnt work well with --eval... so oh well
	private static final boolean FORCE_SCRIPT_FILE = true;

	protected NodeJSCodeBridgeService() {}

	@Override
	public ProcessLFP start(CodeBridgeContext context) throws IOException {
		var packageNames = context.getPackageNames();
		context = context.toBuilder().packageNames(Utils.Lots.stream(packageNames).append("esm").distinct().toList())
				.build();
		return super.start(context);
	}

	@Override
	protected void installPackage(File directory, String packageName) throws IOException {
		try {
			npmInstall(directory, packageName);
		} catch (InvalidExitValueException | InterruptedException | TimeoutException | ExecutionException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
	}

	@SuppressWarnings("unused")
	@Override
	protected ProcessLFP createProcess(File directory, String code, List<String> arguments) throws IOException {
		StreamEx<String> commandStream = Utils.Lots.stream(arguments);
		ProcessListener processListener;
		if (!FORCE_SCRIPT_FILE && !code.contains("\"") && code.length() < 255) {
			// dummy 'run' must be passed to allow for other arguments. idk man
			commandStream = commandStream.prepend("node", "--expose-gc", "--require", "esm", "--eval", code, "run");
			processListener = null;
		} else {
			var scriptFile = new File(directory, String.format("script_%s.js", Utils.Crypto.getSecureRandomString()));
			processListener = new ProcessListener() {

				@Override
				public void beforeStart(ProcessExecutor executor) {
					Throws.unchecked(() -> {
						Files.writeString(scriptFile.toPath(), code, Utils.Bits.getDefaultCharset());
					});
				}

				@Override
				public void afterStart(Process process, ProcessExecutor executor) {
					process.onExit().whenComplete((v, t) -> scriptFile.delete());
				}

			};
			commandStream = commandStream.prepend("node", "--expose-gc", "--require", "esm", scriptFile.getName());
		}
		var pe = Procs.processExecutor().systemEnvironmentVariables().command(commandStream.toList());
		pe.logOutput();
		pe.directory(directory);
		if (processListener != null)
			pe.addListener(processListener);
		var process = pe.start();
		return process;
	}

	public void npmInstall(File directory, String packageName)
			throws InvalidExitValueException, IOException, InterruptedException, TimeoutException, ExecutionException {
		File file = moduleToFile(packageName).orElse(null);
		if (file != null)
			return;
		Optional<File> localModule = getLocalModuleFromGithub(directory, packageName);
		if (localModule.isPresent())
			packageName = localModule.get().getAbsolutePath();
		var envMap = Utils.Lots.stream(System.getenv()).filterKeys(Predicate.not("CLASSPATH"::equals))
				.toCustomMap(LinkedHashMap::new);
		var process = Procs.start(String.format("npm install %s", packageName), pe -> {
			pe.directory(directory);
			pe.environment(envMap);
			pe.logOutput();
			pe.exitValueNormal();
		});
		process.getFuture().get();
	}

	protected Optional<File> getLocalModuleFromGithub(File directory, String module) throws IOException {
		URI gitURI = URIs.parse(module).orElse(null);
		if (gitURI == null || !"github.com".equalsIgnoreCase(TLDs.parseDomainName(gitURI.getHost())))
			return Optional.empty();
		String path = gitURI.getPath();
		path = Utils.Strings.stripStart(path, "/");
		path = Utils.Strings.stripEnd(path, "/");
		String[] pathChunks = path == null ? new String[0] : path.split(Pattern.quote("/"));
		Validate.isTrue(pathChunks.length >= 2, "invalid github uri (no path):%s", gitURI);
		String user = pathChunks[0];
		String project = pathChunks[1];
		Validate.isTrue(Utils.Strings.isNotBlank(user) && Utils.Strings.isNotBlank(project),
				"invalid github uri (no user or proj):%s", gitURI);
		URI downloadURI = URI.create(String.format("https://github.com/%s/%s/archive/master.zip", user, project));
		File targetDir = new File(
				directory.getAbsolutePath() + "/github-download/" + Base58.encodeBytes(Utils.Crypto.hashMD5(gitURI)));
		File zipFile = new File(targetDir, "download.zip");
		zipFile.getParentFile().mkdirs();
		zipFile.createNewFile();
		logger.info("downloading github project. uriPath:{} destinationZip:{}", downloadURI.getPath(),
				zipFile.getAbsolutePath());
		var req = HttpRequests.request().uri(downloadURI).header(Headers.USER_AGENT, UserAgents.CURL.get());
		var resp = HttpClients.send(req);
		try (var is = resp.body(); OutputStream os = new FileOutputStream(zipFile)) {
			StatusCodes.validate(resp.statusCode(), 2);
			Utils.Bits.copy(is, os);
		}
		ZipUtil.unpack(zipFile, targetDir);
		String installPath = Utils.Lots.stream(pathChunks).skip(4).prepend(project + "-master").joining("/");
		File installDir = new File(targetDir, installPath);
		Validate.isTrue(installDir.exists() && installDir.isDirectory(),
				"unable to find install directory:%s module:%s", installDir, module);
		// zipFile.delete();
		return Optional.of(installDir);
	}

	private static Optional<File> moduleToFile(String module) {
		URI uri = URIs.parse(module).orElse(null);
		if (uri != null)
			return Optional.empty();
		File file = new File(module);
		if (file == null || !file.exists())
			file = Utils.Resources.getResourceFile(module);
		if (file == null || !file.exists())
			return Optional.empty();
		if (file.isDirectory()) {
			Boolean allClassFiles = null;
			for (var subFile : file.listFiles()) {
				var ext = Utils.Strings.substringAfterLast(subFile.getName(), ".");
				if (Utils.Strings.equalsIgnoreCase("class", ext))
					allClassFiles = true;
				else {
					allClassFiles = false;
					break;
				}
			}
			if (Boolean.TRUE.equals(allClassFiles))
				return Optional.empty();
		}
		return Optional.of(file);
	}

	public static void main(String[] args)
			throws IOException, IllegalArgumentException, InterruptedException, ExecutionException {
		var process = NodeJSCodeBridgeService.get().start(blder -> {
			blder.packageNames("random-name", "flexsearch");
			blder.code("import random from 'random-name'\n" + "  console.log(random())");
			blder.refreshAfter(Duration.ofMinutes(1));
		});
		process.getFuture().get();
	}

}
