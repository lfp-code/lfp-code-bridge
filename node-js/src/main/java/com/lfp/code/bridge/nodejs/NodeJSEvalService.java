package com.lfp.code.bridge.nodejs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.List;

import com.google.gson.JsonElement;
import com.lfp.code.bridge.grpc.GRPCEvalService;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public class NodeJSEvalService extends GRPCEvalService<NodeJSCodeBridgeService> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final boolean DEBUG_ENABLED = MachineConfig.isDeveloper() && true;
	private static final String SERVER_FILE_NAME = "grpc.server.js";
	private static final List<String> BASE_PACKAGES = List.of("@grpc/grpc-js", "@grpc/proto-loader", "argparse",
			"promise-workers");

	public NodeJSEvalService(String... packageNames) {
		super(NodeJSCodeBridgeService.get(), null, -1, Utils.Crypto.getSecureRandomString(),
				MachineConfig.isDeveloper() ? Duration.ofDays(1) : Duration.ofDays(7), packageNames);
	}

	@Override
	protected InputStream getCode() throws IOException {
		if (MachineConfig.isDeveloper()) {
			var file = Utils.Resources.getResourceFile(SERVER_FILE_NAME);
			if (file != null) {
				var path = file.getAbsolutePath();
				path = Utils.Strings.replace(path, "\\target\\classes\\", "\\src\\main\\resources\\");
				file = new FileExt(path);
				if (file.exists())
					return new FileInputStream(file);
			}
		}
		return Utils.Resources.getResourceInputStream(SERVER_FILE_NAME);
	}

	@Override
	protected Iterable<? extends Object> getArguments(File protoFile) throws IOException {
		var serverAddress = this.getServerAddress();
		var password = this.getPassword().orElse(null);
		var argumentStream = Utils.Lots.stream("--host", serverAddress.getHostString(), "--port",
				serverAddress.getPort(), "--password", password, "--protoPath", protoFile.getAbsolutePath(), "--debug",
				DEBUG_ENABLED);
		return argumentStream;
	}

	@Override
	protected Iterable<? extends String> getPackageNames(String... packageNames) throws IOException {
		return Utils.Lots.stream(packageNames).prepend(BASE_PACKAGES);
	}

	public static void main(String[] args) throws Exception {
		try (var service = new NodeJSEvalService("google-play-scraper")) {
			{
				var result = service
						.eval("var gplay = require('google-play-scraper');\r\n" + "\r\n"
								+ "return gplay.app({appId: 'com.google.android.apps.translate'});", JsonElement.class)
						.block();
				System.out.println(Serials.Gsons.getPretty().toJson(result));
			}
			try {
				var result = service
						.eval("var gplay = require('google-play-scraper');\r\n" + "\r\n" + "return 'hello there'",
								JsonElement.class)
						.block();
				System.out.println(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
			service.eval("console.log(\"supdood\");", Void.class);
			for (int i = 0; i < 10; i++) {
				var je = service.eval("return 69", JsonElement.class).block();
				System.out.println(je);
			}
			System.out.println("call complete");
		}
	}
}
