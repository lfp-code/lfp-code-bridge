const {
	RSocketServer,
	BufferEncoders
} = require('rsocket-core');
const RSocketTCPServer = require('rsocket-tcp-server').default;
const {
	Flowable
} = require('rsocket-flowable');
const {
	Worker
} = require('worker_threads');
const {
	ArgumentParser
} = require('argparse');
const assert = require('assert');

//utils

const bufferToString = (buffer) => {
	if (buffer == null)
		return null;
	return buffer.toString()
}

const objectToPayload = (obj) => {
	return {
		data: objectToBuffer(obj),
		metadata: null, // or new Buffer(...)
	}
}

const objectToBuffer = (obj) => {
	if (obj == null)
		return null;
	var json = JSON.stringify(obj);
	debugLog(() => `objectToBuffer:${json}`)
	return Buffer.from(json)
}

//arguments
const parser = new ArgumentParser();
parser.add_argument('--host', {
	help: 'host'
});
parser.add_argument('--port', {
	help: 'port'
});
parser.add_argument('--password', {
	help: 'password'
});
parser.add_argument('--debug', {
	help: 'debug'
});
args = parser.parse_args();

const host = args['host'];
const port = Number.parseInt(args['port']);
const debug = args['debug'] != null && args['debug'].toLowerCase() == 'true';
const password = args['password'];

const transportOpts = {
	host: host,
	port: port
};
const transport = new RSocketTCPServer(transportOpts, BufferEncoders);

//helpers

const authenticate = (payload, logError) => {
	if (!password) {
		debugLog('auth ok, no password set');
		return;
	}
	if (password === bufferToString(payload.metadata)) {
		debugLog('auth ok, password match');
		return;
	}
	var error = new Error('not authorized');
	if (!!logError)
		error = logError(error);
	throw error;
}

const debugLog = function(v) {
	if (debug) {
		if (typeof v === 'function')
			v = v();
		if (typeof v === 'object')
			v = JSON.stringify(v);
		console.log(v);
	}
	return v;
}

const logError = function(err) {
	if (err != null)
		console.log(err);
	return err;
};

const getWorkerCode = (payload) => {
	var payloadCode = bufferToString(payload.data);
	var code = `
		const {
			parentPort
		} = require('worker_threads');
		var javaCallback = (v) => parentPort.postMessage({
			callback: v
		})
		var javaListeners = [];
		parentPort.on('message', (v) => {
			var message = JSON.parse(v);
			for (var listener of javaListeners)
				if (listener != null)
					listener(message);
		})
		var javaListener = (listener) => javaListeners.push(listener);
		const resultPromise = new Promise((resolve, reject) => {
				var func = () => {
					//eval payloadCode
					${payloadCode}
				};
				resolve(func());
			});
		resultPromise.then((v) => parentPort.postMessage({
				response: v
			}), (e) => {
			if (!(e instanceof Error))
				e = new Error(e);
			parentPort.postMessage(e)
		});
				`;
	return code;
}

const getWorker = (workerCode, onResponse, onError, onCallback) => {
	//console.log(code)
	assert(workerCode != null && workerCode.length > 0)
	var worker = new Worker(workerCode, {
		eval: true
	});
	worker.on('message', v => {
		if (v instanceof Error) {
			if (onError != null)
				onError(v);
			else
				logError(v);
		} else if (v.callback != null) {
			if (onCallback != null)
				onCallback(v);
		} else {
			if (onResponse != null)
				onResponse(v);
		}
	})
	return worker;
}

const fireAndForget = (payload) => {
	authenticate(payload, true)
	getWorker(getWorkerCode(payload))
}

const requestChannel = (payloads) => {
	var javaSubscriberComplete = false;
	var javaSubscriber;
	var workerComplete = false;
	var worker;
	var terminate = (error) => {
		debugLog('terminate');
		logError(error)
		if (worker != null && !workerComplete) {
			worker.terminate();
			workerComplete = true;
		}
		if (javaSubscriber != null && !javaSubscriberComplete) {
			if (error != null)
				javaSubscriber.onError(error);
			else
				javaSubscriber.onComplete();
			javaSubscriberComplete = true;
		}
	}
	var workerCode;
	const responseFlux = new Flowable(sub => {
		javaSubscriber = sub;
		var onResponse = (v) => {
			debugLog(() => {
				return {
					method: 'onResponse',
					data: v
				};
			});
			if (!javaSubscriberComplete)
				javaSubscriber.onNext(objectToPayload(v));
			terminate();
		};
		var onError = (v) => {
			debugLog('worker error');
			terminate(v);
		};
		var onCallback = (v) => {
			debugLog(() => {
				return {
					method: 'onCallback',
					data: v
				};
			});
			if (!javaSubscriberComplete)
				javaSubscriber.onNext(objectToPayload(v));
		};
		// lambda is not executed until `subscribe()` is called
		javaSubscriber.onSubscribe({
			cancel: () => terminate(),
			request: n => {
				if (worker == null) {
					debugLog('worker init');
					worker = getWorker(workerCode, onResponse, onError, onCallback);
				}

			}
		});
	});
	var nodeSubscriber;
	payloads.subscribe({
		onComplete: () => debugLog('request payloads complete'),
		onError: error => terminate(error),
		// Nothing happens until `request(n)` is called
		onSubscribe: sub => {
			nodeSubscriber = sub;
			nodeSubscriber.request(1);
		},
		onNext: payload => {
			debugLog('request payload');
			authenticate(payload);
			if (workerCode == null) {
				workerCode = getWorkerCode(payload);
			} else if (worker != null)
				worker.postMessage(bufferToString(payload.data));
			nodeSubscriber.request(1);
		}
	});
	return responseFlux;
}

const getRequestHandler = (requestingRSocket, setupPayload) => {
	//console.log(`setupPayload.data:${bufferToString(setupPayload.data)} setupPayload.metadata:${bufferToString(setupPayload.metadata)}`)
	return {
		fireAndForget: fireAndForget,
		requestChannel: requestChannel
	};

}

const rSocketServer = new RSocketServer({
	transport,
	getRequestHandler
});

rSocketServer.start();
console.log(`RSocket node server started on port ${host}:${port}`);
