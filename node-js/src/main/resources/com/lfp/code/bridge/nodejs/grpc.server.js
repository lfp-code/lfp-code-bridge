const { ArgumentParser } = require('argparse');
const { PromiseWorker } = require('promise-workers')
const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');



//arguments
const parser = new ArgumentParser();
parser.add_argument('--host', {
	help: 'host'
});
parser.add_argument('--port', {
	help: 'port'
});
parser.add_argument('--password', {
	help: 'password'
});
parser.add_argument('--debug', {
	help: 'debug'
});
parser.add_argument('--protoPath', {
	help: 'protoPath'
});
args = parser.parse_args();

//consts

const HOST = args['host'];
const PORT = Number.parseInt(args['port']);
const DEBUG_LOG = args['debug'] != null && args['debug'].toLowerCase() == 'true';
const PASSWORD = args['password'];
const PROTO_PATH = args['protoPath'];
const NADA = Buffer.from(Math.random().toString()).toString('base64').substr(10, 16);
const CODE_TOKEN_REGEX = /\/\/.*CODE_TOKEN/;

//configure proto

const packageDefinition = protoLoader.loadSync(
	PROTO_PATH, {
	keepCase: true,
	longs: String,
	enums: String,
	defaults: true,
	oneofs: true
});
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
const grpcBridge = protoDescriptor.com.lfp.code.bridge.grpc.bridgeservice;

function debugLog(v) {
	if (DEBUG_LOG) {
		if (v && {}.toString.call(v) === '[object Function]')
			v = v();
		if (typeof v === 'object')
			v = JSON.stringify(v);
		console.log(v);
	}
	return v;
}

const workerFunctionTemplate = () => {
	var resolve = arguments[0];
	var reject = arguments[1];
	var workerData = arguments[2];
	try {
		var codeFunction = () => {
			//CODE_TOKEN
		};
		var resultPromise = Promise.resolve(codeFunction()).then(v => {
			if (v == null)
				v = workerData.nullResult;
			return v;
		});
		resolve(resultPromise);
	} catch (e) {
		console.error(e);
		reject(e);
	}
};

function getWorkerPromise(code) {
	var workerFunctionCode = workerFunctionTemplate.toString();
	workerFunctionCode = workerFunctionCode.substring(workerFunctionCode.indexOf('{') + 1, workerFunctionCode.lastIndexOf('}')).trim();
	workerFunctionCode = workerFunctionCode.replace(CODE_TOKEN_REGEX, code);
	//debugLog(() => `getWorkerPromise. workerFunctionCode:${workerFunctionCode} type:${typeof workerFunctionCode}`);
	return new PromiseWorker((resolve, reject) => {
		var fn = new Function(workerData.workerFunctionCode);
		fn(resolve, reject, workerData);
	}, {
		workerData: {
			workerFunctionCode: workerFunctionCode,
			nullResult: NADA
		}
	});
}


function authenticate(call) {
	callPassword = call.metadata.get('password');
	if (callPassword != null && !(typeof callPassword === 'string'))
		callPassword = callPassword + '';
	if (!PASSWORD) {
		debugLog('auth ok, no password set');
		return;
	}
	if (PASSWORD === callPassword) {
		debugLog('auth ok, password match');
		return;
	}
	debugLog('auth failure, callPassword:${callPassword}');
	throw new Error('not authorized')
}


function onNext(call, responseData) {
	if (responseData === undefined || responseData === NADA)
		responseData = null;
	debugLog(() => `onComplete. responseData:${responseData} type:${typeof responseData}`)
	responsePayload = { data: JSON.stringify(responseData) };
	return call.write(responsePayload);
}

function onError(call, error) {
	if (error === undefined || error == null)
		error = new Error('unknown error');
	else if (!(error instanceof Error))
		error = new Error(error + '');
	console.log(error);
	responsePayload = {
		error: true,
		data: JSON.stringify(error.message)
	};
	call.write(responsePayload);
}



function onRequest(call, request) {
	var responsePromise;
	try {
		//process request
		authenticate(call);
		requestData = JSON.parse(request.data);
		responsePromise = getWorkerPromise(requestData);
	} catch (e) {
		responsePromise = Promise.reject(e);
	}
	responsePromise.then(responseData => {
		return Promise.resolve(onNext(call, responseData));
	}).catch(e => {
		onError(call, e);
	}).finally(() => {
		call.end();
	});
}


function invoke(call) {
	call.on('data', request => onRequest(call, request));
	call.on('end', function() {
		debugLog('client end (worker may continue)')
	});
}

var server = new grpc.Server();
server.addService(grpcBridge.BridgeService.service, {
	invoke: invoke
});
server.bindAsync(`${HOST}:${PORT}`, grpc.ServerCredentials.createInsecure(), (err) => {
	if (err != null) {
		console.error(err);
		process.exit(1);
	}
	server.start();
	console.log(`gRPC server started. address:${HOST}:${PORT}`);
});


