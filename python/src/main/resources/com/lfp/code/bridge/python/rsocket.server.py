import sys
import asyncio
import json
import argparse

from rsocket import RSocket, BaseRequestHandler
from rsocket.payload import Payload
from reactivestreams.publisher import Publisher
from unbounded_thread_pool import UnboundedThreadPoolExecutor

# Construct the argument parser
ap = argparse.ArgumentParser()
ap.add_argument('--host', required=True,
   help='host')
ap.add_argument('--port', required=True,
   help='port')
ap.add_argument('--password', help='password')
args = vars(ap.parse_args())
host =args['host']
port = int(args['port'])
password = args['password']
pool = UnboundedThreadPoolExecutor()

def authenticate(payload):
    if not password:
        return False;
    metadata =payload.metadata
    if metadata is not None and password == metadata.decode():
        return True;
    return False;

def runCode(payload):
    if not authenticate(payload):
          raise Exception('unauthorized')
    code=payload.data.decode()
    code='\t'+code
    code=code.replace('\n', '\n\t')
    code='def func():'+'\n'+code
    code=code+'\n'+'result_ref=func()'
    #print(code)
    loc = {}
    exec(code, globals(), loc)
    result = {'response': loc['result_ref']}
    #print(result)
    return Payload(str.encode(json.dumps(result)),
           None)


	
class Handler(BaseRequestHandler):

    def request_fire_and_forget(self, payload: Payload):
        #print('received request_fire_and_forget: ', payload.data.decode())
        future = pool.submit(runCode,payload)
		
    def request_response(self, payload: Payload) -> asyncio.Future:
        #print('received request_response: ', payload.data.decode())
        future = pool.submit(runCode, payload)
        return asyncio.wrap_future(future)

def session(reader, writer):
    RSocket(reader, writer, handler_factory=Handler)


if __name__ == '__main__':
    try:
        loop = asyncio.get_event_loop()
        service = loop.run_until_complete(asyncio.start_server(
        session, host, port))
        print('started rsocket server: - '+str(host)+':'+str(port))
        loop.run_forever()
    finally:
        print('complete: - '+str(host)+':'+str(port))
        service.close()
        loop.close()
