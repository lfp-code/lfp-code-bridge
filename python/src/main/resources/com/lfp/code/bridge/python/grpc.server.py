from concurrent import futures
import logging
import math
import time

import grpc
import bridge_service_pb2
import bridge_service_pb2_grpc

import sys
import asyncio
import json
import argparse

from unbounded_thread_pool import UnboundedThreadPoolExecutor

# Construct the argument parser
ap = argparse.ArgumentParser()
ap.add_argument('--host', required=True,
   help='host')
ap.add_argument('--port', required=True,
   help='port')
ap.add_argument('--password', help='password')
ap.add_argument('--debug', help='debug')
args = vars(ap.parse_args())

HOST = args['host']
PORT = int(args['port'])
PASSWORD = args['password']
DEBUG = args['debug']

pool = UnboundedThreadPoolExecutor()


def isDebug():
    if not DEBUG:
        return False;
    if DEBUG.lower() == 'true':
        return True;
    return False;


def printDebug(*argv):
    if not isDebug():
        return;
    print(*argv)


def authenticate(context):
    if not PASSWORD:
        return True;
    metadict = dict(context.invocation_metadata())
    requestPassword = metadict['password']
    if requestPassword is not None and PASSWORD == requestPassword:
        return True;
    return False;


def runCode(code):
    code = '\t' + code
    code = code.replace('\n', '\n\t')
    code = 'def func():' + '\n' + code
    code = code + '\n' + 'result_ref=func()'
    printDebug('code:')
    printDebug(code)
    loc = {}
    exec(code, globals(), loc)
    data = loc['result_ref']
    printDebug('data:', data)
    dataJson = json.dumps(data)
    printDebug('dataJson:', dataJson)
    return bridge_service_pb2.Response(
                data=dataJson
    )


class BridgeServiceServicer(bridge_service_pb2_grpc.BridgeServiceServicer):

    def invoke(self, request_iterator, context):
        if not authenticate(context):
          raise Exception('unauthorized')
        for request in request_iterator:
            dataJson = request.data
            data = json.loads(dataJson)
            yield runCode(data)
        printDebug('invoke done')


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    bridge_service_pb2_grpc.add_BridgeServiceServicer_to_server(
        BridgeServiceServicer(), server)
    addr = HOST + ':' + str(PORT)
    server.add_insecure_port(addr)
    server.start()
    print('server started. addr:' + addr)
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()
