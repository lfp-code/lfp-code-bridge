package com.lfp.code.bridge.python;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.time.StopWatch;

import com.lfp.code.bridge.CodeBridgeContext;
import com.lfp.code.bridge.grpc.GRPCEvalService;
import com.lfp.code.bridge.python.PythonEvalService.PythonCodeBridgeServiceExt;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.utils.Utils;

import one.util.streamex.LongStreamEx;

public class PythonEvalService extends GRPCEvalService<PythonCodeBridgeServiceExt> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final boolean DEBUG_ENABLED = MachineConfig.isDeveloper() && true;
	private static final int VERSION = 0;
	private static final List<String> BASE_PACKAGES = List.of("grpcio", "unbounded_thread_pool");
	private static final Map<String, String> BASE_PACKAGE_NAME_OVERRIDE = Map.of();
	private static final String SERVER_FILE_NAME = "grpc.server.py";

	public PythonEvalService(String... packages) {
		super(PythonCodeBridgeServiceExt.get(), null, -1, Utils.Crypto.getSecureRandomString(),
				MachineConfig.isDeveloper() ? Duration.ofDays(1) : Duration.ofDays(7), packages);
	}

	@Override
	protected InputStream getCode() throws IOException {
		var serverFile = Utils.Resources.getResourceFile(SERVER_FILE_NAME);
		return new FileInputStream(serverFile);
	}

	@Override
	protected Iterable<? extends Object> getArguments(File protoFile) throws IOException {
		var serverAddress = this.getServerAddress();
		var password = this.getPassword().orElse(null);
		var argumentStream = Utils.Lots.stream("--host", serverAddress.getHostString(), "--port",
				serverAddress.getPort(), "--password", password, "--debug", DEBUG_ENABLED);
		return argumentStream;
	}

	@Override
	protected Iterable<? extends String> getPackageNames(String... packageNames) throws IOException {
		return Utils.Lots.stream(packageNames).prepend(BASE_PACKAGES)
				.map(v -> BASE_PACKAGE_NAME_OVERRIDE.getOrDefault(v, v));
	}

	protected static class PythonCodeBridgeServiceExt extends PythonCodeBridgeService {

		public static PythonCodeBridgeServiceExt get() {
			return Instances.get();
		}

		protected PythonCodeBridgeServiceExt() {

		}

		@Override
		protected void configureDirectory(CodeBridgeContext context, File directory) throws IOException {
			super.configureDirectory(context, directory);
			var scriptFile = Utils.Resources.getResourceFile(SERVER_FILE_NAME);
			var fileStream = Utils.Lots.stream(
					FileUtils.iterateFiles(scriptFile.getParentFile(), TrueFileFilter.TRUE, TrueFileFilter.TRUE));
			fileStream = fileStream.filter(v -> {
				for (var check : List.of("pb2.py", "pb2_grpc.py")) {
					if (Utils.Strings.endsWith(v.getName(), check))
						return true;
				}
				return false;
			});
			fileStream = fileStream.distinct();
			for (var file : fileStream) {
				FileUtils.copyFile(file, new File(directory, file.getName()));
			}
		}

		@Override
		protected String packageHashValue(String packageName) {
			var hashValue = super.packageHashValue(packageName);
			return hashValue + "_" + VERSION;
		}
	}

	public static void main(String[] args) throws IllegalArgumentException, InterruptedException, IOException {
		List<Long> times = new ArrayList<>();
		for (int i = 0; i < 1_000 + 1; i++) {
			StopWatch sw = i == 0 ? null : StopWatch.createStarted();
			Utils.Resources.getResourceAsString(SERVER_FILE_NAME).get();
			if (sw != null)
				times.add(sw.getNanoTime());
		}
		System.out.println(LongStreamEx.of(times).average().getAsDouble());
		PythonEvalService service = new PythonEvalService("amazoncaptcha");
		var last = service.eval("from amazoncaptcha import AmazonCaptcha\r\n"
				+ "link = 'https://images-na.ssl-images-amazon.com/captcha/usvmgloq/Captcha_kwrrnqwkph.jpg'\r\n"
				+ "captcha = AmazonCaptcha.fromlink(link)\r\n" + "solution = captcha.solve()\r\n" + "return solution",
				String.class).block();
		if (last != null)
			System.out.println(last);
		service.scrap();
		System.exit(0);
	}

}
