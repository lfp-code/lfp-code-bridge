package com.lfp.code.bridge.python;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import org.apache.commons.lang3.Validate;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.listener.ProcessListener;

import com.lfp.code.bridge.CodeBridgeService;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.process.ProcessExecutorLFP;
import com.lfp.joe.process.ProcessLFP;
import com.lfp.joe.process.Procs;
import com.lfp.joe.process.Which;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class PythonCodeBridgeService extends CodeBridgeService.Abs {

	public static PythonCodeBridgeService get() {
		return Instances.get();
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String PYTHONUNBUFFERED_ENV = "PYTHONUNBUFFERED";
	private static final String PYTHONPATH_ENV = "PYTHONPATH";
	private static final String PIP_NAME = "pip";
	private static final String EXEC_NAME = "python";
	private static final String EXEC_NAME_WINDOWS = String.format("%s.exe", EXEC_NAME);
	private static final MemoizedSupplier<String> PYTHON_COMMAND_S = Utils.Functions.memoize(() -> {
		List<Stream<File>> locations = new ArrayList<>();
		locations.add(StreamEx.of(new File(String.format("/usr/bin/%s3", EXEC_NAME))));
		locations.add(StreamEx.of(new File(String.format("C:\\Python38\\%s", EXEC_NAME_WINDOWS))));
		locations.add(Utils.Lots.defer(() -> {
			var c = new File("C:\\");
			var fileStream = Utils.Lots.stream(c.exists() ? c.listFiles() : null);
			fileStream = fileStream.filter(File::isDirectory);
			fileStream = fileStream.filter(v -> {
				return Utils.Strings.startsWithIgnoreCase(v.getName(), EXEC_NAME);
			});
			return fileStream.map(v -> new File(v, EXEC_NAME_WINDOWS));
		}));
		locations.add(Utils.Lots.defer(() -> {
			var op = Which.get(String.format("%s3", EXEC_NAME));
			return Utils.Lots.stream(op.stream());
		}));
		locations.add(Utils.Lots.defer(() -> {
			var op = Which.get(EXEC_NAME);
			return Utils.Lots.stream(op.stream());
		}));
		try (var locationsStream = Utils.Lots.flatMap(locations).nonNull();) {
			for (var location : locationsStream) {
				if (!location.exists())
					continue;
				if (!location.canExecute())
					continue;
				return location.getAbsolutePath();
			}
		}
		return EXEC_NAME;
	});

	protected PythonCodeBridgeService() {

	}

	@Override
	protected void installPackage(File directory, String packageName) throws IOException {
		pipInstall(directory, packageName);
	}

	@Override
	protected ProcessLFP createProcess(File directory, String code, List<String> arguments) throws IOException {
		var commandStream = Utils.Lots.stream(arguments);
		var scriptFile = new File(directory, String.format("script_%s.py", Utils.Crypto.getSecureRandomString()));
		var processListener = new ProcessListener() {

			@Override
			public void beforeStart(ProcessExecutor executor) {
				Utils.Functions.unchecked(() -> {
					Files.writeString(scriptFile.toPath(), code, Utils.Bits.getDefaultCharset());
				});
			}

			@Override
			public void afterStart(Process process, ProcessExecutor executor) {
				process.onExit().whenComplete((v, t) -> scriptFile.delete());
			}

		};
		commandStream = commandStream.prepend(PYTHON_COMMAND_S.get(), scriptFile.getAbsolutePath());
		var pe = Procs.processExecutor().systemEnvironmentVariables().command(commandStream.toList());
		pe.logOutput();
		pe.directory(directory);
		pe.addListener(processListener);
		var process = pe.start();
		return process;
	}

	private static boolean pipInstall(File directory, String packageName) throws IOException {
		Validate.notBlank(packageName);
		var additionalCommands = StreamEx.of("-m", PIP_NAME, "install");
		if (directory != null)
			additionalCommands = additionalCommands.append(String.format("--target=.", directory.getAbsolutePath()));
		additionalCommands = additionalCommands.append(packageName);
		{// install
			var pe = createProcessExecutor(PYTHON_COMMAND_S.get(), directory, additionalCommands.toArray(Object.class));
			pe.logOutput();
			Utils.Functions.unchecked(() -> pe.start().get());
		}
		{// verify
			var proc = startIsPackageInstalled(packageName, directory);
			var pr = Utils.Functions.unchecked(() -> proc.get());
			if (pr.getExitValue() == 0)
				return true;
			var errorMessage = String.format("post install check failed. packageName:%s exitValue:%s", packageName,
					pr.getExitValue());
			throw new IllegalStateException(errorMessage);
		}
	}

	private static ProcessLFP startIsPackageInstalled(String packageName, File directory) throws IOException {
		Validate.notBlank(packageName);
		var script = getIsPackageInstalledScript(packageName);
		var pe = createProcessExecutor(PYTHON_COMMAND_S.get(), directory, "-c", script);
		if (MachineConfig.isDeveloper())
			pe.logOutput();
		return pe.start();
	}

	private static String getIsPackageInstalledScript(String packageName) {
		packageName = PythonPackageNameLookup.INSTANCE.apply(packageName);
		return String.format("import pkgutil; exit(not pkgutil.find_loader('%s'))", packageName);
	}

	private static ProcessExecutorLFP createProcessExecutor(String command, File directory,
			Object... additionalCommands) {
		Validate.notBlank(command);
		var commands = Utils.Lots.stream(additionalCommands).nonNull().map(Object::toString).prepend(command).toList();
		if (MachineConfig.isDeveloper()) {
			// logger.info("running commands:{}", commands);
		}
		var pe = Procs.processExecutor().command(commands);
		pe.environment(PYTHONUNBUFFERED_ENV, Objects.toString(1));
		if (directory != null && directory.exists()) {
			pe.directory(directory);
			String pythonPath = System.getenv(PYTHONPATH_ENV);
			pythonPath = Utils.Lots.stream(directory.getAbsolutePath(), pythonPath).filter(Utils.Strings::isNotBlank)
					.joining(":");
			pe.environment(PYTHONPATH_ENV, pythonPath);
		}
		return pe;
	}

	public static void main(String[] args) throws IllegalArgumentException, InterruptedException, IOException {
		var code = "from amazoncaptcha import AmazonCaptcha\r\n"
				+ "link = 'https://images-na.ssl-images-amazon.com/captcha/usvmgloq/Captcha_kwrrnqwkph.jpg'\r\n"
				+ "captcha = AmazonCaptcha.fromlink(link)\r\n" + "solution = captcha.solve()\r\n" + "print(solution)";
		var process = PythonCodeBridgeService.get().start(blder -> {
			blder.packageNames("amazoncaptcha");
			blder.code(code);
			blder.refreshAfter(Duration.ofSeconds(50));
		});
		process.join();
	}

}
