package com.lfp.code.bridge.python;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.regex.Pattern;

import org.apache.commons.lang3.Validate;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.writerloader.ByteStreamSerializers;
import com.lfp.joe.cache.caffeine.writerloader.FileWriterLoader;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

public enum PythonPackageNameLookup implements Function<String, String> {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 2;
	private static final Duration DISK_CACHE_TTL = Duration.ofDays(1);
	private static final Map<String, String> PACKAGE_NAME_MAPPING = Map.of("grpcio", "grpc");
	private final LoadingCache<URI, StatValue<List<String>>> cache;

	private PythonPackageNameLookup() {
		var writerLoader = new FileWriterLoader<URI, StatValue<List<String>>>() {

			@Override
			protected File getFile(@NonNull URI key) throws IOException {
				return Utils.Files.tempFile(THIS_CLASS, VERSION, Utils.Crypto.hashMD5(key).encodeHex());
			}

			@Override
			protected ByteStreamSerializer<URI, StatValue<List<String>>> createByteStreamSerializer() {
				return ByteStreamSerializers.stringList();
			}

			@Override
			protected @Nullable StatValue<List<String>> loadFresh(@NonNull URI key) throws Exception {
				var list = loadFreshFromGithub(key).map(List::of).orElse(List.of());
				return StatValue.build(list);
			}

			@Override
			protected @Nullable Duration getStorageTTL(@NonNull URI key, @NonNull StatValue<List<String>> result) {
				long elapsed = System.currentTimeMillis() - result.getCreatedAt().getTime();
				var remaining = DISK_CACHE_TTL.toMillis() - elapsed;
				remaining = Math.max(remaining, 0);
				return Duration.ofMillis(remaining);
			}
		};
		this.cache = writerLoader.buildCache(Caches.newCaffeineBuilder(-1, null, Duration.ofSeconds(1)));
	}

	@Override
	public String apply(String packageName) {
		packageName = applyInternal(packageName);
		packageName = PACKAGE_NAME_MAPPING.getOrDefault(packageName, packageName);
		return packageName;
	}

	public String applyInternal(String packageName) {
		packageName = Utils.Strings.trim(packageName);
		Validate.notBlank(packageName);
		if (!Utils.Strings.startsWithIgnoreCase(packageName, "git+"))
			return packageName;
		var url = Utils.Strings.substringAfter(packageName, "://");
		if (Utils.Strings.isBlank(url))
			return packageName;
		var uri = URIs.parse("https://" + url).orElse(null);
		if (uri == null)
			return packageName;
		var sv = this.cache.get(uri);
		return Optional.ofNullable(sv).map(v -> v.getValue()).flatMap(v -> Utils.Lots.stream(v).findFirst())
				.orElse(packageName);
	}

	private static Optional<String> loadFreshFromGithub(URI gitURI) throws IOException {
		var pathParts = URIs.streamPathSegments(gitURI).toList();
		if (pathParts.size() < 2)
			return Optional.empty();
		var lastPathPart = pathParts.get(pathParts.size() - 1);
		int splitAt = lastPathPart.indexOf(".git");
		if (splitAt < 0)
			return Optional.empty();
		lastPathPart = lastPathPart.substring(0, splitAt);
		if (Utils.Strings.isBlank(lastPathPart))
			return Optional.empty();
		var setupURI = URI.create(String.format("https://raw.githubusercontent.com/%s/%s/master/setup.py",
				pathParts.get(pathParts.size() - 2), lastPathPart));
		return parseSetupURI(setupURI);
	}

	private static Optional<String> parseSetupURI(URI setupURI) throws IOException {
		try (var is = HttpClients.getDefault().get(setupURI.toString()).ensureSuccess().asStream().getBody()) {
			return parseSetupURI(is);
		}
	}

	private static Optional<String> parseSetupURI(InputStream is) {
		var lineStream = Utils.Strings.streamLines(is);
		lineStream = lineStream.mapPartial(Utils.Strings::trimToNullOptional);
		var setupPattern = Pattern.compile("setup\\W*\\(");
		var foundSetup = new AtomicBoolean();
		lineStream = lineStream.filter(v -> {
			if (foundSetup.get())
				return true;
			if (setupPattern.matcher(v).find())
				foundSetup.set(true);
			return false;
		});
		var nameStream = lineStream.map(v -> parseName(v));
		nameStream = nameStream.nonNull();
		return nameStream.findFirst();
	}

	private static String parseName(String line) {
		var namePattern = Pattern.compile("name\\W*=");
		var matcher = namePattern.matcher(line);
		if (!matcher.find())
			return null;
		line = line.substring(matcher.end());
		line = Utils.Strings.stripEnd(line, ",");
		if (Utils.Strings.isBlank(line))
			return null;
		for (var quote : List.of("'", "\"")) {
			if (Utils.Strings.startsWith(line, quote) && Utils.Strings.endsWith(line, quote)) {
				line = line.substring(1, line.length() - 1);
				break;
			}
		}
		if (Utils.Strings.isBlank(line))
			return null;
		return line;
	}

	public static void main(String[] args) throws IOException {
		System.out.println(PythonPackageNameLookup.INSTANCE.apply("git+https://github.com/linux-china/rsocket-py.git"));
	}
}
