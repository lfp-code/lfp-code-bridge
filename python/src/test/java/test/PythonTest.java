package test;

import com.lfp.code.bridge.python.PythonEvalService;

public class PythonTest {

	public static void main(String[] args) {
		var service = new PythonEvalService();
		var code = "assert False, \"Manually raised error\"";
		code = "print('sup')\nreturn 20 * 3";
		var flux = service.eval(code, Long.class);
		flux = flux.doOnNext(v -> {
			System.out.println(String.format("response:%s", v));
		});
		flux.block();
		System.exit(0);
	}
}
