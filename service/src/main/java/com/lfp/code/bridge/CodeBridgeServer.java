package com.lfp.code.bridge;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.process.ShutdownNotifier;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.socket.socks.Sockets;
import com.lfp.joe.process.ProcessLFP;
import com.lfp.joe.retry.Retrys;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public interface CodeBridgeServer<CB extends CodeBridgeService> extends Scrapable {

	InetSocketAddress getServerAddress();

	Optional<String> getPassword();

	CB getService();

	public static InetSocketAddress toInetSocketAddress(InetSocketAddress inetSocketAddress) {
		Objects.requireNonNull(inetSocketAddress);
		if (IPs.getLocalIPAddress().equals(inetSocketAddress.getHostString()))
			inetSocketAddress = new InetSocketAddress("localhost", inetSocketAddress.getPort());
		return inetSocketAddress;
	}

	public static InetSocketAddress toInetSocketAddress(String hostname, Integer port) {
		if (Utils.Strings.isBlank(hostname))
			hostname = IPs.getLocalIPAddress();
		if (port == null || port == -1)
			port = Utils.Functions.unchecked(Sockets::allocatePort);
		return toInetSocketAddress(new InetSocketAddress(hostname, port));
	}

	public static abstract class Abs<CB extends CodeBridgeService> extends Scrapable.Impl
			implements CodeBridgeServer<CB> {

		private final AtomicReference<Optional<ProcessLFP>> processStarted = new AtomicReference<>();
		private final CB service;
		private final InetSocketAddress serverAdress;
		private final String password;
		private final Duration refreshAfter;
		private final String[] packageNames;

		public Abs(CB service, String hostname, Integer port, String password, Duration refreshAfter,
				String... packageNames) {
			this(service, toInetSocketAddress(hostname, port), password, refreshAfter, packageNames);
		}

		public Abs(CB service, InetSocketAddress inetSocketAddress, String password, Duration refreshAfter,
				String... packageNames) {
			this.service = Objects.requireNonNull(service);
			this.serverAdress = toInetSocketAddress(inetSocketAddress);
			this.password = password;
			this.refreshAfter = refreshAfter;
			this.packageNames = packageNames;
			AutoCloseable shutdownListener = () -> this.scrap();
			ShutdownNotifier.INSTANCE.addListener(shutdownListener);
			this.onScrap(() -> ShutdownNotifier.INSTANCE.removeListener(shutdownListener));
		}

		@Override
		public InetSocketAddress getServerAddress() {
			if (processStarted.compareAndSet(null, Optional.empty())) {
				try {
					createProcessAndWaitForServer();
				} catch (Throwable t) {
					processStarted.set(null);
					throw Utils.Exceptions.asRuntimeException(t);
				}
			}
			return this.serverAdress;
		}

		private void createProcessAndWaitForServer() throws IOException {
			var process = createProcess(refreshAfter, packageNames);
			this.onScrap(() -> process.cancel(true));
			process.listener(() -> this.scrap());
			processStarted.set(Optional.of(process));
			Retrys.execute(30, Duration.ofSeconds(1), nil -> {
				var open = Sockets.isServerPortOpen(this.serverAdress);
				if (!open)
					throw new IOException("server connection failed:" + this.serverAdress);
				return true;
			});
		}

		protected ProcessLFP createProcess(Duration refreshAfter, String... packageNames) throws IOException {
			var code = Utils.Bits.from(getCode()).encodeCharset(Utils.Bits.getDefaultCharset());
			StreamEx<Object> argumentStream = Utils.Lots.stream(getArguments());
			StreamEx<String> packageNameStream = Utils.Lots.stream(getPackageNames(packageNames));
			return this.getService().start(blder -> {
				blder.code(code);
				blder.arguments(argumentStream.toList());
				blder.packageNames(packageNameStream.toList());
				blder.refreshAfter(refreshAfter);
			});

		}

		@Override
		public Optional<String> getPassword() {
			return Optional.ofNullable(this.password);
		}

		@Override
		public CB getService() {
			return this.service;
		}

		protected abstract InputStream getCode() throws IOException;

		protected abstract Iterable<? extends Object> getArguments() throws IOException;

		protected abstract Iterable<? extends String> getPackageNames(String... packageNames) throws IOException;

	}

}
