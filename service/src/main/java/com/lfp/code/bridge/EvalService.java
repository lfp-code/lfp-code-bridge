package com.lfp.code.bridge;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.Optional;

import org.reactivestreams.Publisher;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.core.function.Scrapable;

import reactor.core.publisher.Mono;

public interface EvalService extends Scrapable {

	default <X> Mono<X> eval(String code, Class<X> classType) {
		return eval(code, classType == null ? null : TypeToken.of(classType));
	}

	default <X> Mono<X> eval(String code, TypeToken<X> typeToken) {
		return eval(EvalRequest.builder(code).build(), typeToken);
	}

	default <X> Mono<X> eval(EvalRequest evalRequest, Class<X> classType) {
		return eval(evalRequest, classType == null ? null : TypeToken.of(classType));
	}

	<X> Mono<X> eval(EvalRequest evalRequest, TypeToken<X> typeToken);

	public static abstract class Abs<CB extends CodeBridgeService, R> extends CodeBridgeServer.Abs<CB>
			implements EvalService {

		public Abs(CB service, String hostname, Integer port, String password, Duration refreshAfter,
				String... packageNames) {
			this(service, CodeBridgeServer.toInetSocketAddress(hostname, port), password, refreshAfter, packageNames);
		}

		public Abs(CB service, InetSocketAddress inetSocketAddress, String password, Duration refreshAfter,
				String... packageNames) {
			super(service, inetSocketAddress, password, refreshAfter, packageNames);
		}

		@Override
		public <X> Mono<X> eval(EvalRequest evalRequest, TypeToken<X> typeToken) {
			if (evalRequest == null)
				return Mono.error(new NullPointerException("eval request required"));
			if (this.isScrapped())
				return Mono.error(new IllegalStateException("service has been scrapped"));
			try {
				var flux = Optional.ofNullable(evalInternal(evalRequest)).map(Mono::from).orElseGet(Mono::empty);
				return flux.flatMap(v -> {
					try {
						return Mono.justOrEmpty(mapResponse(v, typeToken));
					} catch (Throwable t) {
						return Mono.error(t);
					}
				});
			} catch (Throwable t) {
				return Mono.error(t);
			}
		}

		protected abstract Publisher<R> evalInternal(EvalRequest evalRequest) throws IOException;

		protected abstract <X> X mapResponse(R input, TypeToken<X> typeToken) throws Exception;

	}

}
