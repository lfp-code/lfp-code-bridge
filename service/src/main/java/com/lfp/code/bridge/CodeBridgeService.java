package com.lfp.code.bridge;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.process.ProcessLFP;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public interface CodeBridgeService {

	default ProcessLFP start(Consumer<CodeBridgeContext.Builder> contextConfig) throws IOException {
		var blder = CodeBridgeContext.builder();
		if (contextConfig != null)
			contextConfig.accept(blder);
		return start(blder.build());
	}

	ProcessLFP start(CodeBridgeContext context) throws IOException;

	public static abstract class Abs implements CodeBridgeService {

		private static final Class<?> THIS_CLASS = new Object() {
		}.getClass().getEnclosingClass();
		private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
		private static final int VERSION = 10;

		protected String packageHashValue(String packageName) {
			if (Utils.Strings.isBlank(packageName))
				return null;
			List<Callable<String>> attempts = new ArrayList<>();
			attempts.add(() -> {
				return Optional.of(new File(packageName)).filter(File::exists).filter(File::isDirectory)
						.map(File::getAbsolutePath).orElse(null);
			});
			attempts.add(() -> {
				return URIs.parse(packageName).map(Object::toString).orElse(null);
			});
			attempts.add(() -> packageName);
			for (var attempt : attempts) {
				var result = Utils.Functions.catching(attempt, t -> null);
				result = Utils.Strings.trimToNull(result);
				if (result != null)
					return result;
			}
			return null;
		}

		@Override
		public ProcessLFP start(CodeBridgeContext context) throws IOException {
			var directory = generateDirectory(context);
			Utils.Files.configureDirectoryLocked(directory, (nil, elapsed) -> {
				if (context.getRefreshAfter().isPresent()
						&& elapsed.toMillis() > context.getRefreshAfter().get().toMillis())
					return false;
				return true;
			}, nil -> {
				configureDirectory(context, directory);
			});
			var proc = createProcess(directory, context.getCode(), context.streamArguments().toList());
			return proc;
		}

		protected File generateDirectory(CodeBridgeContext context) {
			StreamEx<Object> packageNameHashParts = Utils.Lots.stream(context.getPackageNames())
					.map(this::packageHashValue);
			var packageNameHash = generateDirectoryHash(packageNameHashParts);
			return Utils.Files.tempFile(this.getClass(), VERSION, packageNameHash.encodeHex());
		}

		protected Bytes generateDirectoryHash(StreamEx<Object> directoryHashParts) {
			return Utils.Crypto.hashMD5(directoryHashParts.toArray());
		}

		protected void configureDirectory(CodeBridgeContext context, File directory) throws IOException {
			for (var packageName : context.getPackageNames())
				installPackage(directory, packageName);
		}

		protected abstract void installPackage(File directory, String packageName) throws IOException;

		protected abstract ProcessLFP createProcess(File directory, String code, List<String> arguments)
				throws IOException;
	}
}
